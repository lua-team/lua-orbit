lua-orbit (2.2.1+dfsg-2) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Bump debhelper from deprecated 8 to 10.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Use secure URI in Vcs control header Vcs-Git.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

 -- Debian Janitor <janitor@jelmer.uk>  Sat, 03 Dec 2022 08:26:42 -0000

lua-orbit (2.2.1+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Clarify license name in debian/copyright

 -- Enrico Tassi <gareuselesinge@debian.org>  Sun, 02 Feb 2014 10:26:44 +0100

lua-orbit (2.2.0+gita6fb46e+dfsg-1) unstable; urgency=low

  * Clean up orig tarballs to comply to DFSG

 -- Enrico Tassi <gareuselesinge@debian.org>  Tue, 13 Aug 2013 10:18:11 +0200

lua-orbit (2.2.0+gita6fb46e-1) unstable; urgency=low

  * New upstream snapshot fix bugs w.r.t. lua-lpeg

 -- Enrico Tassi <gareuselesinge@debian.org>  Tue, 01 May 2012 15:18:29 +0200

lua-orbit (2.2.0+dfsg1-1) unstable; urgency=low

  * Add jquery-1.2.3.js to the tarball and mention it in README.source and
    in copyright

 -- Enrico Tassi <gareuselesinge@debian.org>  Tue, 01 May 2012 15:15:30 +0200

lua-orbit (2.2.0+dfsg-1) unstable; urgency=low

  * New upstream release
  * Update watch file
  * Switch to dh-lua
  * Packages renamed according to the new Lua policy
  * debian/compat set to 8

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 30 Apr 2012 16:52:06 +0200

lua-orbit (2.1.0+dfsg-3) unstable; urgency=low

  * source format 3.0 (quilt)
  * bumped standards-version to 3.9.2, no changes needed

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 23 Jul 2011 10:26:21 +0200

lua-orbit (2.1.0+dfsg-2) unstable; urgency=low

  * added conflicts/replaces in control (Closes: 557053)

 -- Enrico Tassi <gareuselesinge@debian.org>  Thu, 19 Nov 2009 12:48:20 +0100

lua-orbit (2.1.0+dfsg-1) unstable; urgency=low

  * new upstream release
  * added README.source
  * bumped standards-version to 3.8.3, no changes
  * bumped API version to 1, since the new release alters the API
  * fixed watch file

 -- Enrico Tassi <gareuselesinge@debian.org>  Tue, 17 Nov 2009 20:26:50 +0100

lua-orbit (2.0.2-3) unstable; urgency=low

  * bumped standards-version to 3.8.2, no changes needed
  * build depend on lua5.1-policy-dev

 -- Enrico Tassi <gareuselesinge@debian.org>  Tue, 21 Jul 2009 17:13:00 +0200

lua-orbit (2.0.2-2) unstable; urgency=low

  * depend over wsapi API/ABI version 1

 -- Enrico Tassi <gareuselesinge@debian.org>  Fri, 27 Mar 2009 15:36:38 +0100

lua-orbit (2.0.2-1) unstable; urgency=low

  * new upstream release
  * bumped standards-version to 3.8.1, no changes needed
  * added Depends over misc:Depends

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 23 Mar 2009 10:48:35 +0100

lua-orbit (2.0.1-1) unstable; urgency=medium

  * new upstream release fixing a minor bug

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 11 Jun 2008 21:43:24 +0200

lua-orbit (2.0.0-1) unstable; urgency=medium

  * new upstream release featuring better documentation

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 07 Jun 2008 12:09:30 +0200

lua-orbit (2.0~rc1-2) unstable; urgency=low

  * Added dependency over liblua5.1-policy-dev (Closes: #479939)

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 07 May 2008 11:54:05 +0200

lua-orbit (2.0~rc1-1) unstable; urgency=low

  * Initial release. (Closes: #462659)

 -- Enrico Tassi <gareuselesinge@debian.org>  Thu, 17 Apr 2008 11:11:55 +0200
